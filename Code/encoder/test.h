#ifndef __TEST_H__
#define __TEST_H__

#include <Arduino.h>
#include "encoder.h"
#include <stdlib.h>
#include <stdint.h>

long long int convertToLongLongInt(uint8_t *buffer);

void testPayloadEncoder();

#endif
