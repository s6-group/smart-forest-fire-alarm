#include "test.h" 

long long int convertToLongLongInt(uint8_t* buffer, uint8_t size) {
  long long int value;
  for (int i = 0; i < size; i++) {
    value |= ((long long int)buffer[i]) << (8 * (size - 1) - (8 * i));
  }
  return value;
}
void testPayloadEncoder()
{
    PayloadEncoder encoder(64);
    uint8_t errors = 0;
    uint8_t buffer[3] = {0b01111100,0b00011011};
    if((uint16_t)convertToLongLongInt(buffer, 2) != 0b0111110000011011)
    {
        Serial.println("Error: convertToLongLongInt(buffer) failed");
        return;
    }
    encoder.insert8(-1);
    if((uint8_t)convertToLongLongInt(encoder.getPayload(), 1) != 0b11111111)
    {
        Serial.println("Error: encodeUint8(-1) failed");
        errors++;
    }
    encoder.clear();
    encoder.insert8(0);
    if((uint8_t)convertToLongLongInt(encoder.getPayload(), 1) != 0b0000000)
    {
        Serial.println("Error: encodeUint8(0) failed");
        errors++;
    }
    encoder.clear();
    encoder.insert8(255);
    if(convertToLongLongInt(encoder.getPayload(), 1) != 0b11111111)
    {
        Serial.println("Error: encodeUint8(255) failed");
        errors++;
    }
    encoder.clear();
    encoder.insert16(-1);
    if(convertToLongLongInt(encoder.getPayload(), 2) != 0b01111111111111111)
    {
        Serial.println("Error: encodeUint16(-1) failed");
        errors++;
    }
    encoder.clear();
    encoder.insert16(0);
    if(convertToLongLongInt(encoder.getPayload(), 2) != 0b0000000000000000)
    {
        Serial.println("Error: encodeUint16(0) failed");
        errors++;
    }
    encoder.clear();
    encoder.insert16(65535);
    if(convertToLongLongInt(encoder.getPayload(), 2) != 0b1111111111111111)
    {
        Serial.println("Error: encodeUint16(65535) failed");
        errors++;
    }
    encoder.clear();
    encoder.insert32(-1);
    if(convertToLongLongInt(encoder.getPayload(), 4) != 0b11111111111111111111111111111111)
    {
        Serial.println("Error: encodeUint32(-1) failed");
        errors++;
    }
    encoder.clear();
    encoder.insert32(0);
    if(convertToLongLongInt(encoder.getPayload(), 4) != 0b00000000000000000000000000000000000000)
    {
        Serial.println("Error: encodeUint32(0) failed");
        errors++;
    }
    encoder.clear();
    encoder.insert32(4294967295);
    if(convertToLongLongInt(encoder.getPayload(), 4) != 0b11111111111111111111111111111111)
    {
        Serial.println("Error: encodeUint32(4294967295) failed");
        errors++;
    }
    encoder.clear();
    encoder.insert8(12);
    encoder.insert16(12345);
    encoder.insert32(1234567891);
    if(convertToLongLongInt(encoder.getPayload(), 7) != 0b1100001100000011100101001001100101100000001011010011)
    {
        Serial.println("Error: encodeUint8(12), encodeUint16(12345), encodeUint32(1234567891) failed");
        errors++;
    }
    encoder.clear();
    if(errors == 0)
    {
        Serial.println("All tests passed");
    }
    else
    {
        Serial.print("Failed ");
        Serial.print(errors);
        Serial.println(" tests");
    }
}
