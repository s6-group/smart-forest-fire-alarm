#include "encoder.h"

void PayloadEncoder::insert8(uint8_t data) {
  buffer[bytePosition] = data;
  bytePosition++;
}

void PayloadEncoder::insert16(uint16_t data) {
  insert8((data >> 8) & 0xFF); //MSB first
  insert8(data & 0xFF);
}

void PayloadEncoder::insert32(uint32_t data) {
  insert16((data >> 16) & 0xFFFF); //MSB first
  insert16(data & 0xFFFF);
}

void PayloadEncoder::clear() {
  memset(buffer, 0, PAYLOAD_BUFFER_SIZE);
  bytePosition = 0;
}

void PayloadEncoder::addPayload(uint8_t versionNumber, double voltage_, uint8_t fireIndex, double temp_, uint8_t humid, uint16_t co2, 
    uint32_t gps){
      clear();
      insert8(versionNumber);
      uint16_t combined = 0;
      uint16_t voltage = (uint16_t)(voltage_ * 1000);
      combined = (voltage << 2);
      combined |= fireIndex;
      insert16(combined);      

      combined = 0;
      uint16_t temp = (uint16_t)temp_;
      combined = (temp << 7);
      combined |= humid;
      insert16(combined);      

      insert16(co2);
      insert32(gps);
    }
