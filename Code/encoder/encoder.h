/**
 * @file encoder.h
 *
 * @brief Contains the declaration of the PayloadEncoder class.
 * @author Henry Vrugteveen
 *
 */

#ifndef __ENCODER_H__
#define __ENCODER_H__

#include <stdint.h>
#include <Arduino.h>

#define PAYLOAD_BUFFER_SIZE 11

/**
 * @brief A class for encoding payload data.
 *
 * This class provides methods for inserting various types of data into a payload buffer, as well as
 * for retrieving the encoded payload data.
 */
class PayloadEncoder {
  private:
    uint8_t buffer[PAYLOAD_BUFFER_SIZE]; ///< The payload buffer.
    uint16_t bytePosition;  ///< The current position in the payload buffer.

  public:
    /**
     * @brief Constructor for the PayloadEncoder class.
     */
    PayloadEncoder(void){};

    /**
     * @brief Destructor for the PayloadEncoder class.
     */
    ~PayloadEncoder(){ clear();};

    /**
     * @brief Inserts an 8-bit value into the payload buffer.
     *
     * @param data The value to insert.
     */
    void insert8(uint8_t data);

    /**
     * @brief Inserts a 16-bit value into the payload buffer.
     *
     * @param data The value to insert.
     */
    void insert16(uint16_t data);

    /**
     * @brief Inserts a 32-bit value into the payload buffer.
     *
     * @param data The value to insert.
     */
    void insert32(uint32_t data);

    /**
     * @brief Clears the payload buffer.
     */
    void clear();

    /**
     * @brief Gets the size of the payload buffer.
     *
     * @return The size of the payload buffer.
     */
    const uint16_t getPayloadSize() { return PAYLOAD_BUFFER_SIZE;}

    /**
     * @brief Gets the payload buffer.
     *
     * @return The payload buffer.
     */
    uint8_t* getPayload() { return buffer; };

    /**
     * @brief Adds payload data to the payload buffer.
     *
     * @param versionNumber The version number of the payload data.
     * @param voltage_ The voltage value to add.
     * @param fireIndex The fire index to add.
     * @param temp_ The temperature value to add.
     * @param humid The humidity value to add.
     * @param co2 The CO2 value to add.
     * @param gps The GPS value to add.
     */
    void addPayload(uint8_t versionNumber, double voltage_, uint8_t fireIndex, double temp_, uint8_t humid, uint16_t co2, 
    uint32_t gps);
};

#endif
