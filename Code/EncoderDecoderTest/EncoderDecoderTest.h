/// \author Antonios Gkougkoulidis
/// \file EncoderDecoderTest.h



#ifndef __ENCODERDECODERTEST_H
#define __ENCODERDECODERTEST_H


#include <Arduino.h>
#include <stdint.h>
#include "encoder.h"
#include "Decoder.h"

/// @brief tests the Encoder and Decoder by filling a buffer using the Encoder 
/// class and then Decoding the buffer using the Decoder class
void EncoderDecoderTest();



#endif