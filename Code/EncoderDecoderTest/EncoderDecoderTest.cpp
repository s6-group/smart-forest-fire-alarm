#include "EncoderDecoderTest.h"

void EncoderDecoderTest()
{
    PayloadEncoder EncoderPayload;
    PayloadDecoder DecoderPayload;

    uint8_t version = 100;
    double Voltage = 12.20;
    uint8_t FireIndex = 10;
    double Temperature = 27.10;
    uint8_t Humidity = 15;
    uint16_t CO2 = 1500;
    int32_t GPS_latitude = 12500;
    int32_t GPS_longitude = -12500;


    EncoderPayload.addPayload(version, Voltage, FireIndex, Temperature, Humidity, CO2, GPS_latitude, GPS_longitude);

    DecoderPayload.set_Payload_buf(EncoderPayload.getPayload());
    DecoderPayload.set_Payload_Size(EncoderPayload.getPayloadSize());
    DecoderPayload.decode_payload();

      Serial.println("Test Decoded values:");
  Serial.print("Version: ");
  if(DecoderPayload.get_version() == version)
    Serial.println("PASS");
  else
    Serial.println("FAIL");

  Serial.print("Voltage: ");
  if(DecoderPayload.get_Voltage() == Voltage)
    Serial.println("PASS");
  else
    Serial.println("FAIL");

  Serial.print("Fire index: ");
  if(DecoderPayload.get_Fire_index() == FireIndex)
    Serial.println("PASS");
  else
    Serial.println("FAIL");

  Serial.print("Temperature reading: ");
  if(DecoderPayload.get_temp_reading() == round(Temperature))
    Serial.println("PASS");
  else
    Serial.println("FAIL");

  Serial.print("Humidity reading: ");
  if(DecoderPayload.get_humidity_reading() == Humidity)
    Serial.println("PASS");
  else
    Serial.println("FAIL");

  Serial.print("CO2 reading: ");
  if(DecoderPayload.get_CO2_reading() == CO2)
    Serial.println("PASS");
  else
    Serial.println("FAIL");

  Serial.print("GPS Latitude: ");
  if(DecoderPayload.get_GPS_Latitude() == double(GPS_latitude) / 10000)
    Serial.println("PASS");
  else
    Serial.println("FAIL");

  Serial.print("GPS Longitude: ");
  if(DecoderPayload.get_GPS_Longitude() == double(GPS_longitude) / 10000)
    Serial.println("PASS");
  else
    Serial.println("FAIL");


}
