/// \author Antonios Gkougkoulidis
/// \file Decoder.h
/// \brief Payload Decoder class

#ifndef DECODER_H
#define DECODER_H

#include<stdint.h>
#include<Arduino.h>

/// \brief Payload Decoder class
/// The class is used to decode the variables out of the payload
class PayloadDecoder {
public:
   /// \brief Class Constructor
   PayloadDecoder(){}

   /// \brief Class Destructor
   ~PayloadDecoder(){}

   /// \brief Sets the pointer to the buffer that has the payload
   void set_Payload_buf(uint8_t *payload) {buffer = payload;}

   /// \brief Sets the payload size in bytes
   /// \param size The payload buffer size in bytes
   void set_Payload_Size (uint8_t size) {buffersize = size;}

   /// \brief Decodes the payload and puts the values inside the class member variables
   void decode_payload();

   /// \brief Returns the version number of the payload in the correct type
   int get_version() const {return Version;}

   /// \brief Returns the Voltage of the battery with a precision of 3 decimals
   float get_Voltage() const {return Voltage;}

   /// \brief Returns the Fire index based on sensor readings in the correct type
   int get_Fire_index() const { return Fire_index;}

   /// \brief Returns the temperature reading variable value in the correct type
   float get_temp_reading() const {return temp_reading;}

   /// \brief Returns the CO2 reading variable value in the correct type
   unsigned int get_CO2_reading() const {return CO2_reading;}

   /// \brief Returns the Humidity reading variable value in the correct type
   int get_humidity_reading() const {return humidity_reading;}

   /// \brief Returns the GPS Latitude value in the correct type 
   float get_GPS_Latitude() const {return GPS_Latitude;}

   /// \brief Returns the GPS Longitude value in the correct type
   float get_GPS_Longitude() const {return GPS_Longitude;}

private:
   uint8_t *buffer;
   uint8_t buffersize;

   int Version;
   double Voltage;
   int Fire_index;
   double temp_reading;
   int humidity_reading;
   unsigned int CO2_reading;
   double GPS_Latitude;
   double GPS_Longitude;


   /// \brief Decodes payload bytes to uint32_t format
   /// \param buf The buffer containing the payload
   /// \param pos The position inside the buffer where the uint32_t starts
   uint32_t decode_uint32 (uint8_t *buf, uint8_t pos);

   /// \brief Decodes payload bytes to uint16_t format
   /// \param buf The buffer containing the payload
   /// \param pos The position inside the buffer where the uint16_t starts
   uint16_t decode_uint16 (uint8_t *buf, uint8_t pos);

   /// \brief Decodes payload bytes to uint8_t format
   /// \param buf The buffer containing the payload
   /// \param pos The position inside the buffer where the uint8_t starts
   uint8_t  decode_uint8  (uint8_t *buf, uint8_t pos);
};



#endif // DECODER_H
