function decodeUplink(input) {
    // Extract the payload bytes from the input object
    const bytes = input.bytes;
  
  //checks if the humidity sensor value is within range
    if(decode_uint16(bytes,4) & 127 > 100){
      return {
        data:{},
        warnings:[],
        errors:["Humididty sensor reading above range"]
      };
    }
    
    //checks if the payload length is correct
    if(bytes.length != 16){
      return {
        data:{},
        warnings:[],
        errors:["Payload length is wrong"]
      };
    }
    //checks if the CO2 sensor reading is above range
      if(decode_uint16(bytes,6) > 5000){
      return {
        data:{},
        warnings:[],
        errors:["CO2 sensor reading above range"]
      };
    }
    
    
  
    // Return an object with the decoded data, warnings, and errors
    return {
      data: {
        Version: bytes[0],
        FireIndex: bytes[1],
        Voltage: decode_uint16(bytes,2) / 1000 ,
        temperature: (decode_uint16(bytes,4) >> 7),
        Humidity: decode_uint16(bytes,4) & 127,
        CO2: decode_uint16(bytes,6),
        GPS1: decode_uint32(bytes,8),
        GPS2: decode_uint32(bytes,12),
        
      },
      warnings: [],
      errors: []
    };
  }
  
  function decode_uint16(payload,id) {
    let value = 0;
    
    value = (payload[id] << 8) | payload [id+1];
    
    return value;
  }
  
  function decode_uint32(payload,id) {
    let value = 0;
    
    for (let i=0; i<4; i++) {
    value |= (payload[id+i] << (24-(i*8)));  // msb
    }
    
    return value;
  }
