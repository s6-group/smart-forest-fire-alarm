#include "Decoder.h"

void PayloadDecoder::decode_payload()
{
   Version = static_cast<int>(decode_uint8(buffer,0));
   Fire_index = static_cast<int>(decode_uint16(buffer,1));
   Voltage = static_cast<float>(decode_uint16(buffer,2)) / 1000;
   temp_reading = static_cast<float>(decode_uint16(buffer,4) >> 7);
   humidity_reading  = static_cast<int>(decode_uint16(buffer,4) & 0b01111111);
   CO2_reading = static_cast<unsigned int>(decode_uint16(buffer,6));
   GPS_Latitude = static_cast<float>(decode_uint32(buffer,8)) / 10000;
   GPS_Longitude = static_cast<float>(decode_uint32(buffer,12)) / 10000;
}

uint32_t PayloadDecoder::decode_uint32 (uint8_t *buf, uint8_t pos) {
   uint32_t value {0};
   for (uint8_t i=0; i<4; i++) {
     value |= ((uint32_t)buf[pos+i] << (24-(i*8)));  // msb
   }
   return value;
}

uint16_t PayloadDecoder::decode_uint16(uint8_t *buf, uint8_t pos) {
   uint16_t value {0};
   value = ((uint16_t)buf[pos] << 8 | (uint16_t)buf[pos+1]);
  return value;
}

uint8_t PayloadDecoder::decode_uint8(uint8_t *buf, uint8_t pos){
   uint8_t value {0};
   value = (uint8_t)buf[pos];
   return value;
}

