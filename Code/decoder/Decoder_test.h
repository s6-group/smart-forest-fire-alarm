/// \file Decoder_test.h
/// \author Antonios Gkougkoulidis

#ifndef __DECODER_TEST_H
#define __DECODER_TEST_H

#include "Decoder.h"
#include <Arduino.h>
#include <stdlib.h>

/// \brief tests the PayloadDecoder class
void test_PayloadDecoder();


#endif