function decode_uint16(payload,id) {
    var value = 0;
    
    value = (payload[id] << 8) | payload [id+1];
    
    return value;
  }
  
  function decode_uint32(payload,id) {
    var value = 0;
    
    for (var i=0; i<4; i++) {
    value |= (payload[id+i] << (24-(i*8)));  // msb
    }
    
    return value;
  }
  
  function Decoder(bytes, port) {
      // Decode an uplink message from a buffer
      // (array) of bytes to an object of fields.
  
  
      var version = bytes[0];
      var fireindex = bytes[1];
      var voltage = decode_uint16(bytes,2) / 1000;
      var temperature = (decode_uint16(bytes,4) >> 7);
      var humidity = decode_uint16(bytes,4) & 127;
      var CO2 = decode_uint16(bytes,6);
      var gps1 = decode_uint32(bytes,8) / 10000; // latitude
      var gps2 = decode_uint32(bytes,12) / 10000; // longitude
      
      if(version == 1)
          datacakeFields = [
              {
                  field: "BATTERY",
                  value: voltage,
              },
              {
                  field: "MEAN_TEMPERATURE",
                  value: temperature,
              },
              {
                  field: "MEAN_HUMIDITY",
                  value: humidity,
              },
              {
                  field: "CO2_SHT",
                  value: CO2,
              },
              {
                  field: "FIRE_INDEX",
                  value: fireindex,
              },
                            {
                  field: "GPS1",
                  value: gps1,
              },
              {
                  field: "GPS2",
                  value: gps2,
              },
              {
                  field: "GPS",
                  value: "(" + gps1 + "," + gps2 + ")",
              },
          ];
      else
          datacakeFields = [
              {
                  field: "BATTERY",
                  value: voltage,
              },
              {
                  field: "TEMPERATURE",
                  value: temperature,
              },
              {
                  field: "HUMIDITY",
                  value: humidity,
              },
              {
                  field: "CO2_SHT",
                  value: CO2,
              },
              {
                  field: "FIRE_INDEX",
                  value: fireindex,
              },
              {
                  field: "GPS1",
                  value: gps1,
              },
              {
                  field: "GPS2",
                  value: gps2,
              },
              {
                  field: "GPS",
                  value: "(" + gps1 + "," + gps2 + ")",
              },
          ];
      if (normalizedPayload.data_rate && normalizedPayload.gateways && normalizedPayload.gateways[0])  {
          datacakeFields.push({field:"LORA_SNR", value:normalizedPayload.gateways[0].snr});
          datacakeFields.push({field:"LORA_RSSI", value:normalizedPayload.gateways[0].rssi});
          datacakeFields.push({field:"LORA_RSSI", value:normalizedPayload.data_rate});
      } else {
          console.log("No Gateway Metadata found - Skipping")
      }
      
      return datacakeFields;
  }
