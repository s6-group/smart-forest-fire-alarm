#include "Decoder_test.h"



void test_PayloadDecoder() {
    PayloadDecoder pd;
    uint8_t payload[] = {0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
    pd.set_Payload_buf(payload);
    pd.set_Payload_Size(16);
    pd.decode_payload();


  
  Serial.println("Test Decoded values:");
  Serial.print("Version: ");
  if(pd.get_version() == 1)
    Serial.println("PASS");
  else
    Serial.println("FAIL");

  Serial.print("Voltage: ");
  if(pd.get_Voltage() == 65.535)
    Serial.println("PASS");
  else
    Serial.println("FAIL");

  Serial.print("Fire index: ");
  if(pd.get_Fire_index() == 255)
    Serial.println("PASS");
  else
    Serial.println("FAIL");

  Serial.print("Temperature reading: ");
  if(pd.get_temp_reading() == 511)
    Serial.println("PASS");
  else
    Serial.println("FAIL");

  Serial.print("Humidity reading: ");
  if(pd.get_humidity_reading() == 127)
    Serial.println("PASS");
  else
    Serial.println("FAIL");

  Serial.print("CO2 reading: ");
 // Serial.println(pd.get_CO2_reading());
  if(pd.get_CO2_reading() == 65535)
    Serial.println("PASS");
  else
    Serial.println("FAIL");

  Serial.print("GPS Latitude: ");
  Serial.println(pd.get_GPS_Latitude());
  if(pd.get_GPS_Latitude() == -0.0001)

    Serial.println("PASS");
  else
    Serial.println("FAIL");

  Serial.print("GPS Longitude: ");
  Serial.println(pd.get_GPS_Longitude());
  if(pd.get_GPS_Longitude() == -0.0256)
    Serial.println("PASS");
  else
    Serial.println("FAIL");



}