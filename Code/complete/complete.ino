#include "sffa.h"

const char *appEui = "YOUR_APP_EUI";
const char *appKey = "YOUR_APP_KEY";
const float latitude = 52.308718;
const float longitude = 5.626185;

SFFA *sffa;

void setup()
{
  sffa = new SFFA(appEui, appKey, latitude, longitude);
}

void loop()
{
  sffa->loop();
}
