#include "Shield.h"

shieldPotmeter::shieldPotmeter(const pot_pin_t potPin) : potPin_(potPin) {}

float shieldPotmeter::readPot(int min, int max)
{
  int potVal = (potPin_ == pot_pin_t::POT_RED) ? analogRead(PIN_POT_RED) : analogRead(PIN_POT_WHITE);
  int retVal = map(potVal, 0, 1023, min, max);

  return static_cast<float>(retVal);
}

shieldButton::button_state_t shieldButton::readButton(const button_pin_t button)
{
  uint8_t state = (button == button_pin_t::RED_BUTTON) ? digitalRead(PIN_SWITCH_RED) : digitalRead(PIN_SWITCH_BLACK);

  return (state == LOW) ? button_state_t::IS_PRESSED : button_state_t::IS_RELEASED;
}

shieldLed::shieldLed()
{
  pinMode(PIN_LED_1_RED, OUTPUT);
  pinMode(PIN_LED_2_RED, OUTPUT);
  pinMode(PIN_LED_3_GRN, OUTPUT);
  pinMode(PIN_LED_4_GRN, OUTPUT);
}

void shieldLed::writeLed(const uint8_t ledPin, const led_state_t ledState)
{
  if (ledState == led_state_t::LED_ON)
    digitalWrite(ledPin, HIGH);
  else
    digitalWrite(ledPin, LOW);
}