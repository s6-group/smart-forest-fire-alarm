/*!
 * @file FireIndexLite.h
 *
 * @brief This header file contains the declaration of the FireIndexLite class, which calculates a fire index number based on temperature, humidity, and CO2 levels.
 * The class provides methods to calculate the fire index number based on provided values, or based on previously set values.
 * @author Thomas van den Oever
 */

#ifndef FIREINDEXLITE_H
#define FIREINDEXLITE_H
#include <Arduino.h>
#include <math.h>
#include <stdlib.h>

/*!
 * @class fireIndexLite
 * 
 * @brief Class to calculate the fire index number.
 */
class fireIndexLite
{
public:
    /*!
     * @brief Constructor for the FireIndexLite class.
     *
     * @param temp The temperature value for the FireIndexLite object.
     * @param humidity The humidity value for the FireIndexLite object.
     * @param co2 The CO2 value for the FireIndexLite object.
     */
    fireIndexLite(int temp, unsigned int humidity, unsigned int co2) : temp(temp), humidity(humidity), co2(co2) {}

    /**
     * @brief Default constructor for the FireIndexLite class.
     *
     * Initializes temperature, humidity, and CO2 to 0.
     */
    fireIndexLite() : temp(0), humidity(0), co2(0) {}
    ~fireIndexLite() {}

    /*!
     * @brief Calculate a fire index number based on the temperature, humidity and C02 levels.
     *
     * @param temp The temperature in degrees celsius.
     * @param humidity The humidity in relative humidity percentage.
     * @param c02 The C02 level in part per million
     * @return A fire index number where 0 means non of the parameters crossed the threshold. where each parameter crossing their threshold means adding 1 or more depending on the amount that the threshold has been crossed.
     */
    uint8_t calculateFireIndex(int temp_, unsigned int humidity_, unsigned int co2_);

    /*!
     * @brief Calculate a fire index number based on the previous provided numbers
     *
     * @return A fire index number where 0 means non of the parameters crossed the threshold. where each parameter crossing their threshold means adding 1 or more depending on the amount that the threshold has been crossed.
     */
    uint8_t calculateFireIndex();

protected:
    bool getFireWarning();
    uint8_t getFireIndexNumber();

private:
    const struct threshold
    {
        int temp = 60;              ///< Temperature threshold number
        unsigned int humidity = 22; ///< Humidity threshold number
        unsigned int co2 = 1400;    ///< CO2 threshold number
    } threshold;

    const struct divider
    {
        unsigned int temp = 5;     ///< Temperature divider, used to scale the added number
        unsigned int humidity = 5; ///< Humidity divider, used to scale the added number
        unsigned int co2 = 100;    ///< CO2 divider, used to scale the added number
    } divider;

    int temp;                ///< Last used tempature value
    unsigned int humidity;   ///< Last used humidity value
    unsigned int co2;        ///< Last used CO2 value
    bool fireWarning;        ///< Bool is set to true if the calculated fireIndexNumber is higher than 0
    uint8_t fireIndexNumber; ///< uint8_t the last calculated fireIndexNumber
};

#endif // FIREINDEXLITE_H