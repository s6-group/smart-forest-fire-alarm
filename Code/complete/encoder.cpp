#include "encoder.h"

void PayloadEncoder::insert8(uint8_t data)
{
  buffer[bytePosition] = data;
  bytePosition++;
}

void PayloadEncoder::insert16(uint16_t data)
{
  insert8((data >> 8) & 0xFF); // MSB first
  insert8(data & 0xFF);
}

void PayloadEncoder::insert32(uint32_t data)
{
  insert16((data >> 16) & 0xFFFF); // MSB first
  insert16(data & 0xFFFF);
}

void PayloadEncoder::clear()
{
  memset(buffer, 0, PAYLOAD_BUFFER_SIZE);
  bytePosition = 0;
}

void PayloadEncoder::addPayload(uint8_t versionNumber, double voltage_, uint8_t fireIndex, double temp_, uint8_t humid, uint16_t co2,
                                int32_t latitude, int32_t longitude)
{
  clear();
  insert8(versionNumber);
  insert8(fireIndex);
  uint16_t voltage = (uint16_t)(voltage_ * 1000);
  insert16(voltage);

  uint16_t combined = 0;
  uint16_t temp = (uint16_t)temp_;
  combined = (temp << 7);
  combined |= humid;
  insert16(combined);

  insert16(co2);
  insert32(latitude);
  insert32(longitude);
}
