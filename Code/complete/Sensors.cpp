#include "Sensors.h"

tempSensor::tempSensor() : oneWireInterface_(PIN_DALLAS), sensor_(&oneWireInterface_)
{
  sensor_.begin();
}

const float tempSensor::tempRead()
{
  sensor_.requestTemperatures();

  return sensor_.getTempCByIndex(0);
}

humiSensor::humiSensor() : shieldPotmeter(pot_pin_t::POT_RED) {}

uint8_t humiSensor::readHumidity()
{
  return (uint8_t)readPot(0, 100);
}

co2Sensor::co2Sensor() : shieldPotmeter(pot_pin_t::POT_WHITE) {}

uint16_t co2Sensor::readCO2()
{
  return (uint16_t)readPot(750, 2000);
}