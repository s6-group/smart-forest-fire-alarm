#include "sffa.h"

SFFA::SFFA(const char *appEui_, const char *appKey_, const float latitude, const float longitude) : appEui{appEui_}, appKey{appKey_}, TheThingsNetwork(loraSerial, debugSerial, freqPlan, spreadingFactor)
{
  gps = new GPS(latitude, longitude);

  loraSerial.begin(57600);
  debugSerial.begin(9600);
  // Wait a maximum of 10s for Serial Monitor
  while (!debugSerial && millis() < 10000)
    ;

#ifdef ENABLE_LOG
  debugSerial.println("-- STATUS");
  showStatus();
  debugSerial.println("-- JOIN");
#endif
  join(appEui, appKey);
  currentTime = millis();
}

void SFFA::sendFire()
{
  uint8_t versionNumber = 2; // Version 2 is for active fire
  double voltage = 4.32;
  double temp = tempSen.tempRead();
  uint8_t humid = humiSen.readHumidity();
  uint16_t co2 = co2Sen.readCO2();
  uint8_t fireIndex = calculateFireIndex(temp, humid, co2);
  int32_t latitude_ = (int32_t)(gps->getLatitude() * 10000);
  int32_t longitude_ = (int32_t)(gps->getLongitude() * 10000);

  encoder.addPayload(versionNumber, voltage, fireIndex, temp, humid, co2, latitude_, longitude_);

  // Send it off
  sendBytes(encoder.getPayload(), encoder.getPayloadSize());
  encoder.clear();
}

void SFFA::sendDaily()
{
  uint8_t versionNumber = 1; // Version 1 is for normal operation
  double voltage = 4.32;
  uint8_t fireIndex = getFireIndexNumber();
  int32_t latitude_ = (int32_t)(gps->getLatitude() * 10000);
  int32_t longitude_ = (int32_t)(gps->getLongitude() * 10000);

  encoder.addPayload(versionNumber, voltage, fireIndex, meanTemp, meanHumid, meanCO2, latitude_, longitude_);

  sendBytes(encoder.getPayload(), encoder.getPayloadSize());
  encoder.clear();
}

void SFFA::measure()
{
  double temp = tempSen.tempRead();
  uint8_t humid = humiSen.readHumidity();
  uint16_t co2 = co2Sen.readCO2();
  calculateFireIndex(temp, humid, co2); // Update the fire index number
  meanTemp = (meanTemp * measurementsToday + temp) / (measurementsToday + 1);
  meanHumid = (meanHumid * measurementsToday + humid) / (measurementsToday + 1);
  meanCO2 = (meanCO2 * measurementsToday + co2) / (measurementsToday + 1);
  measurementsToday++;
}

void SFFA::clearDaily()
{
  measurementsToday = 0;
  meanTemp = 0;
  meanHumid = 0;
  meanCO2 = 0;
}

void SFFA::measureSend()
{
  measure();

  if (getFireWarning())
  {
    sendFire();
  }
}

void SFFA::loop()
{
#ifdef ENABLE_LOG
  debugSerial.println("Going to sleep");
  delay(100); // Delay so that the serial print can send all data before going back to sleep
#endif
  currentTime += Watchdog.sleep(5000); // Wake up every 5 seconds (the max time to sleep is 8000ms)

  if (currentTime - lastDaily > dailyInterval)
  {
    sendDaily();
    clearDaily();
    lastDaily = millis();
  }

  if (currentTime - lastMeasure > measureInterval)
  {
    measureSend();
    lastMeasure = millis();
#ifdef ENABLE_LOG
    USBDevice.attach();
    delay(5000); // Delay to reconnect USB and resume serial monitor
    debugSerial.print("Temperature: ");
    debugSerial.println(meanTemp);
    debugSerial.print("Humidity: ");
    debugSerial.println(meanHumid);
    debugSerial.print("CO2: ");
    debugSerial.println(meanCO2);
    debugSerial.print("Fire Index: ");
    debugSerial.println(getFireIndexNumber());
    debugSerial.print("GPS: ");
    debugSerial.print((int32_t)(gps->getLongitude() * 10000));
    debugSerial.println((int32_t)(gps->getLatitude() * 100000));


#endif
  }
}
