/*!
 * @file GPS.h
 *
 * @brief Defines the GPS class for obtaining latitude and longitude coordinates.
 * @author Henry Vrugteveen and Thomas van den Oever
 */

#ifndef __GPS__
#define __GPS__
#include <Arduino.h>

/*!
 * @class GPS
 *
 * @brief Represents a GPS location with latitude and longitude coordinates.
 */
class GPS{
public:

  /*!
   * @brief Default constructor for the GPS class.
   *
   * Initializes latitude and longitude to 0.
   */
  GPS();

  /*!
   * @brief Constructor for the GPS class.
   *
   * @param latitude_ The initial latitude value for the GPS object.
   * @param longitude_ The initial longitude value for the GPS object.
   */
  GPS(float latitude_, float longitude_): latitude(latitude_), longitude(longitude_){};

  /*!
   * @brief Returns the current latitude value of the GPS object.
   *
   * @return The current latitude value of the GPS object.
   */
  float getLatitude(){return latitude;}

  /*!
   * @brief Returns the current longitude value of the GPS object.
   *
   * @return The current longitude value of the GPS object.
   */
  float getLongitude(){return longitude;}
private:
  float longitude;  ///< The current longitude value of the GPS object.
  float latitude;   ///< The current latitude value of the GPS object.
};
#endif