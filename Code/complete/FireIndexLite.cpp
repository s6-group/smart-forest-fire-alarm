#include "FireIndexLite.h"

uint8_t fireIndexLite::calculateFireIndex(int temp_, unsigned int humidity_, unsigned int co2_)
{
    temp = temp_;
    humidity = humidity_;
    co2 = co2_;
    uint8_t tempFireIndexNumber = 0;
    if (temp >= threshold.temp)
    {
        tempFireIndexNumber += (temp - threshold.temp) / divider.temp;
    }

    if (humidity <= threshold.humidity)
    {
        tempFireIndexNumber += (threshold.humidity - humidity) / divider.humidity;
    }

    if (co2 >= threshold.co2)
    {
        tempFireIndexNumber += (co2 - threshold.co2) / divider.co2;
    }

    fireWarning = tempFireIndexNumber > 0;
    fireIndexNumber = tempFireIndexNumber;
    return fireIndexNumber;
}

uint8_t fireIndexLite::calculateFireIndex()
{
    return calculateFireIndex(temp, humidity, co2);
}

bool fireIndexLite::getFireWarning()
{
    return fireWarning;
}

uint8_t fireIndexLite::getFireIndexNumber()
{
    return fireIndexNumber;
}