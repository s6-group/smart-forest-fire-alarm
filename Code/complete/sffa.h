/**
 * @file SFFA.h
 *
 * @brief Contains the declaration of the SFFA class.
 * @author Henry Vrugteveen and Thomas van den Oever
 *
 */

#ifndef _SFFA_H_
#define _SFFA_H_

#include <Arduino.h>
#include <TheThingsNetwork.h>
#include <Adafruit_SleepyDog.h>
#include "encoder.h"
#include "Sensors.h"
#include "FireIndexLite.h"
#include "gps.h"

#define ENABLE_LOG ///< Uncomment this line to enable debug logging via the Serial port
#define loraSerial Serial1
#define debugSerial Serial

#define freqPlan TTN_FP_EU868

#define spreadingFactor 9

#define dailyInterval 86400000 / 4 ///< The interval between daily measurements 6 hours in milliseconds).
#define measureInterval 10000  ///< The interval between measurements (10 seconds in milliseconds).

/**
 * @brief A class for handling the SFFA project.
 *
 * This class handles the SFFA project by measuring temperature, humidity, and CO2 levels, sending
 * data to the LoRa network, and processing data for daily reports and fire alerts.
 */
class SFFA : fireIndexLite, TheThingsNetwork
{
private:
  unsigned long currentTime = 0; ///< The current time in milliseconds.
  unsigned long lastMeasure = 0; ///< The time of the last measurement in milliseconds.
  unsigned long lastDaily = 0;   ///< The time of the last daily report in milliseconds.

  const char *appEui; ///< The application EUI for the LoRa network.
  const char *appKey; ///< The application key for the LoRa network.

  PayloadEncoder encoder; ///< The payload encoder object for encoding data for transmission over the LoRa network.
  tempSensor tempSen;     ///< The temperature sensor object for measuring temperature.
  humiSensor humiSen;     ///< The humidity sensor object for measuring humidity.
  co2Sensor co2Sen;       ///< The CO2 sensor object for measuring CO2 levels.
  GPS *gps;               ///< Pointer to the GPS object for getting the gps data.

  uint16_t measurementsToday = 0; ///< The number of measurements taken today.
  double meanTemp;                ///< The mean temperature of today's measurements.
  uint8_t meanHumid;              ///< The mean humidity of today's measurements.
  uint16_t meanCO2;               ///< The mean CO2 level of today's measurements.

protected:
  void measureSend();

  /**
   * @brief Sends a fire payload over the LoRa network.
   */
  void sendFire();

  /**
   * @brief Sends a daily report over the LoRa network.
   */
  void sendDaily();

  /**
   * @brief Measures average temperature, humidity, and CO2 levels.
   */
  void measure();

  /**
   * @brief Clears daily measurement data.
   */
  void clearDaily();

public:
  /**
   * @brief Constructor for the SFFA class.
   *
   * @param appEui_ The application EUI for the LoRa network.
   * @param appKey_ The application key for the LoRa network.
   */
  SFFA(const char *appEui_, const char *appKey_, const float latitude, const float longitude);
  /**
   * @brief Main loop for the SFFA project.
   */
  void loop();
};

#endif
