/*!
 * @file Sensors.h
 * @brief This file contains the declaration of three classes to handle different sensors.
 * Temperature can be read from a tempSensor implemented in the shield.
 * Humidity can be read from a redPotmeter on the shield in percent.
 * CO2 can be read from a whitePotmeter on the shield in 'ppm'.
 * 
 * @author Zein Sabbagh and Thomas van den Oever
 */

#ifndef SENSORS_H__
#define SENSORS_H__

#include "Shield.h"
#include <OneWire.h>
#include <DallasTemperature.h>

/*!
 * @class tempSensor
 * 
 * @brief Class to handle temperature sensor readings.
 */
class tempSensor
{
public:
  static const auto PIN_DALLAS = 2; ///< The pin number used to read temperature.

  /*!
   * @brief Constructor for tempSensor.
   */
  tempSensor();

  /*!
   * @brief Destructor for tempSensor.
   */
  ~tempSensor() = default;

  /*!
   * @brief Reads the temperature from the sensor.
   *
   * @return The temperature in Celsius.
   */
  const float tempRead();

private:
  OneWire oneWireInterface_; ///< OneWire interface for reading temperature.
  DallasTemperature sensor_; ///< DallasTemperature sensor object.
};

/*!
 * @class humiSensor
 *
 * @brief Class to handle humidity sensor readings.
 */
class humiSensor : public shieldPotmeter
{
public:
  /*!
   * @brief Constructor for humiSensor.
   */
  humiSensor();

  /*!
   * @brief Destructor for humiSensor.
   */
  ~humiSensor() = default;

  /*!
   * @brief Reads the humidity from the sensor.
   * @return The humidity in percent.
   */
  uint8_t readHumidity();
};

/*!
 * @class co2Sensor
 *
 * @brief Class to handle humidity sensor readings.
 */
class co2Sensor : public shieldPotmeter
{
public:
  /*!
   * @brief Constructor for co2Sensor.
   */
  co2Sensor();

  /*!
   * @brief Destructor for co2Sensor.
   */
  ~co2Sensor() = default;

  /*!
   * @brief Reads the CO2 level from the sensor.
   * @return The CO2 level in ppm.
   */
  uint16_t readCO2();
};

#endif // SENSORS_H__