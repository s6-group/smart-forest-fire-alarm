/*!
 * @file Shield.h
 * @brief 
 * 
 * @author Zein Sabbagh and Thomas van den Oever
 */

#ifndef SHIELD_H__
#define SHIELD_H__

#include <Arduino.h>

/*!
 * @class shieldPotmeter
 *
 * @brief This class represents the shield potentiometer used to measure humidity and CO2.
 */
class shieldPotmeter
{
public:
  static const auto PIN_POT_RED = A0;   ///< The pin of the potentiometer to read from.
  static const auto PIN_POT_WHITE = A1; ///< The white potentiometer pin.

  enum class pot_pin_t
  {
    POT_RED,
    POT_WHITE
  };

  /*!
   * @brief Construct a new shieldPotmeter object.
   * 
   * @param potPin The pin of the potentiometer to read from.
   */
  shieldPotmeter(const pot_pin_t potPin);

  /*!
   * @brief Destroy the shieldPotmeter object.
   */
  ~shieldPotmeter() = default;

  /*!
   * @brief Read the value of the potentiometer.
   * 
   * @param min The minimum value of the potentiometer.
   * @param max The maximum value of the potentiometer.
   * @return float The value of the potentiometer.
   */
  float readPot(int min, int max);

private:
  const pot_pin_t potPin_; ///< The pin of the potentiometer to read from.
};

/*!
 * @class shieldButton
 * @brief This class represents the shield button used to control the LED's.
 */
class shieldButton
{
public:
  static const auto PIN_SWITCH_BLACK = 8; ///< The black button pin.
  static const auto PIN_SWITCH_RED = 9;   ///< The red button pin.

  enum class button_pin_t
  {
    BLACK_BUTTON,
    RED_BUTTON
  };

  enum class button_state_t
  {
    IS_PRESSED, ///< The button is pressed.
    IS_RELEASED ///< The button is released.
  };

  shieldButton() = default;
  ~shieldButton() = default;

  /*!
   * @brief Read the state of the button.
   * 
   * @param button The button to read from.
   * @return button_state_t The state of the button.
   */
  button_state_t readButton(const button_pin_t button);
};

/*!
 * @class shieldLed
 * @brief This class represents the shield LED's.
 */
class shieldLed
{
public:
  static const auto PIN_LED_1_RED = 3;
  static const auto PIN_LED_2_RED = 4;
  static const auto PIN_LED_3_GRN = 5;
  static const auto PIN_LED_4_GRN = 6;

  enum class led_state_t
  {
    LED_ON, ///< The LED is on.
    LED_OFF ///< The LED is off.
  };

  shieldLed();
  ~shieldLed() = default;

  /*!
   * @brief Write to a specific LED.
   * 
   * @param ledPin The pin of the LED to write to.
   * @param ledState The state of the LED.
   */
  void writeLed(const uint8_t ledPin, const led_state_t ledState);
};

#endif // SHIELD_H__