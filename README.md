# Smart Forest Fire Alarm

This is a smart forest fire alarm based on the Arduino Leonardo with a Sensor Shield. An assignment for the IoT lab class.

## Usage
Open the  `Code/complete/complete.ino` file in the [Arduino IDE](https://www.arduino.cc/en/software) and upload to a [Arduino Leonardo](https://docs.arduino.cc/hardware/leonardo) node.

Important!
In the `Code/complete/complete.ino` file needs a `appEui` and  `appKey` before uploading the code to the node

## Documentation
You can generate the documentation by running Doxygen in the root directory of the repository. The documentation will be in the `Docs/DoxygenGenerated` folder.